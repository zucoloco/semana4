#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun  1 14:37:08 2020

@author: gustavo
"""



import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter

rho = np.zeros((100, 100)) 
u = np.zeros((100, 100)) 
v = np.zeros((100, 100)) 

feq = np.zeros((100, 100, 9))
f = np.zeros((100, 100, 9))
f_linha = np.zeros((100, 100, 9)) #para atualizar o vetor na propagação sem sobrepor valores

w = np.array([4/9 , 1/9, 1/9, 1/9, 1/9, 1/36, 1/36, 1/36, 1/36, 1/36])
cx = np.array([0, 1, 0, -1, 0, 1, -1, -1, 1])
cy = np.array([0, 0, 1, 0, -1, 1, 1, -1, -1])

n = 100
m = 100

alpha = 0.01 #viscosidade cinemática
omega = 53/100 #calculei na mão
# Re = uo * 100 / alpha

tmpx = np.zeros(9)
tmpy = np.zeros(9)

for x in range(0, 100):
    u[x, 99] = 0.1
    for y in range(0, 100): 
        rho[x, y] = 5.0 
        
for t in range(10000):
    for x in range(0,100):
        for y in range(0,100):             
            feq[x, y, 0] = rho[x, y] * w[0] * (1 - 1.5 * (u[x, y] * u[x, y] + v[x,y] * v[x,y]))
            feq[x, y, 1] = rho[x, y] * w[1] * (1 + 3 * u[x, y] + 4.5 * u[x, y] * u[x, y] - 1.5 * (u[x, y] * u[x, y] + v[x, y] * v[x, y])) 
            feq[x, y, 2] = rho[x, y] * w[2] * (1 + 3 * v[x, y] + 4.5 * v[x, y] * v[x, y] - 1.5 * (u[x, y] * u[x, y] + v[x, y] * v[x, y]))
            feq[x, y, 3] = rho[x, y] * w[3] * (1 - 3 * u[x, y] + 4.5 * u[x, y] * u[x, y] - 1.5 * (u[x, y] * u[x, y] + v[x, y] * v[x, y]))
            feq[x, y, 4] = rho[x, y] * w[4] * (1 - 3 * v[x, y] + 4.5 * v[x, y] * v[x, y] - 1.5 * (u[x, y] * u[x, y] + v[x, y] * v[x, y]))
            feq[x, y, 5] = rho[x, y] * w[5] * (1 + 3 * (u[x, y] + v[x, y]) + 4.5 * (u[x, y] + v[x, y]) * (u[x, y] + v[x, y]) - 1.5 * (u[x, y] * u[x, y] + v[x, y] * v[x, y]))
            feq[x, y, 6] = rho[x, y] * w[6] * (1 + 3 * (v[x, y] - u[x, y]) + 4.5 * (v[x, y] - u[x, y]) * (v[x, y] - u[x, y]) - 1.5 * (u[x, y] * u[x, y] + v[x, y] * v[x, y]))
            feq[x, y, 7] = rho[x, y] * w[7] * (1 + 3 * (-u[x, y] - v[x, y]) + 4.5 * (u[x, y] + v[x, y]) * (u[x, y] + v[x, y]) - 1.5 * (u[x, y] * u[x, y] + v[x, y] * v[x, y]))
            feq[x, y, 8] = rho[x, y] * w[8] * (1 + 3 * (u[x, y] - v[x, y]) + 4.5 * (u[x, y] - v[x, y])*(u[x, y] - v[x, y]) - 1.5 * (u[x, y] * u[x, y] + v[x, y] * v[x, y]))
            
            for k in range(0, 9):
                f[x, y, k] = f[x, y, k] * (1 - omega) + omega * feq[x, y, k] #colisão        
            
    for x in range(0,100):
        for y in range(0,100):
            rho[x, y] = sum(f[x, y, : ]) 
            rho[x, 99] = f[x, 99, 0] + f[x, 99, 1] + f[x, 99, 3] + 2 * (f[x, 99, 2] + f[x, 99, 6] + f[x, 99, 5])
            
            for k in range(0, 9):
                tmpx[k] = f[x,y,k] * cx[k]
                tmpy[k] = f[x,y,k] * cy[k]
            
            u[x,y] = (1 / rho[x,y]) * sum(tmpx[:])
            v[x,y] = (1 / rho[x,y]) * sum(tmpy[:])
            u[x, 99] = 0.1 # u_lid = 6m/s, mas u_lbm = 0.1

    for y in range(99, 0, -1):
    	for x in range(0, 99):
    		f_linha[x, y, 2] = f[x, y-1, 2] 
    		f_linha[x, y, 6] = f[x+1, y-1, 6] 
    
    for y in range(99, 0, -1):
    	for x in range(99, 0, -1):
    		f_linha[x, y, 1] = f[x-1, y, 1]
    		f_linha[x, y, 5] = f[x-1, y-1, 5]
    
    for y in range(0, 99):
    	for x in range(99, 0, -1):
    		f_linha[x, y, 4] = f[x, y+1, 4]
    		f_linha[x, y, 8] = f[x-1, y+1, 8]
     
    for y in range(0, 99):
    	for x in range(0, 99):
    		f_linha[x, y, 3] = f[x+1, y, 3]
    		f_linha[x, y, 7] = f[x+1, y+1, 7]
    
    
    for p in range(0,100):
    	#bounce back on west boundary
    	f_linha[0, p, 1] = f[0, p, 3]
    	f_linha[0, p, 5] = f[0, p, 7]
    	f_linha[0, p, 8] = f[0, p, 6]

    	#bounce back on east boundary
    	f_linha[99, p, 6] = f[99, p, 8]
    	f_linha[99, p, 3] = f[99, p, 1]
    	f_linha[99, p, 7] = f[99, p, 5]
        
    	#bounce back on south boundary
    	f_linha[p, 0, 2] = f[p, 0, 4]
    	f_linha[p, 0, 6] = f[p, 0, 8]
    	f_linha[p, 0, 5] = f[p, 0, 7]

    #movin lid, north boundary    
    for i in range(1, 99):
        rho_n = f[i, 99, 0] + f[i, 99, 1] + f[i, 99, 3] + 2 * (f[i, 99, 2] + f[i, 99, 6] + f[i, 99, 5])
        f_linha[i, 99, 4] = f[i, 99, 2] 
        f_linha[i, 99, 8] = f[i, 99, 6] + rho_n * 0.1 / 6.0
        f_linha[i, m-1, 7] = f[i, m-1, 5] - rho_n * 0.1 / 6.0
                
    tmp = f
    f = f_linha
    f_linha = tmp 

   
X = np.arange(0, 100, 1)
Y = np.arange(0, 100, 1)
X, Y = np.meshgrid(X, Y)
vel = np.zeros((100,100))

for x in range(0,100):
    for y in range(0,100):
        vel[x, y] = u[x, y] * u[x, y] + v[x, y] * v[x, y] 
        vel[x, y] = np.sqrt(vel[x,y])
   
fig = plt.figure()
ax = fig.gca(projection='3d')

surf = ax.plot_surface(X, Y, vel, cmap=cm.coolwarm,
                       linewidth=0, antialiased=False)

ax.set_zlim(-3.0, 3.0)
ax.zaxis.set_major_locator(LinearLocator(10))
ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

fig.colorbar(surf, shrink=0.5, aspect=5)

plt.show()