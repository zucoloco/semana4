#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun  2 13:53:49 2020

@author: gustavo
"""

import numpy as np

def colisao(u, v, f, feq, rho, omega, w, cx, cy, n, m):
    for x in range(0, n):
            for y in range(0,m):             
                feq[x, y, 0] = rho[x, y] * w[0] * (1 - 1.5 * (u[x, y] * u[x, y] + v[x,y] * v[x,y]))
                feq[x, y, 1] = rho[x, y] * w[1] * (1 + 3 * u[x, y] + 4.5 * u[x, y] * u[x, y] - 1.5 * (u[x, y] * u[x, y] + v[x, y] * v[x, y])) 
                feq[x, y, 2] = rho[x, y] * w[2] * (1 + 3 * v[x, y] + 4.5 * v[x, y] * v[x, y] - 1.5 * (u[x, y] * u[x, y] + v[x, y] * v[x, y]))
                feq[x, y, 3] = rho[x, y] * w[3] * (1 - 3 * u[x, y] + 4.5 * u[x, y] * u[x, y] - 1.5 * (u[x, y] * u[x, y] + v[x, y] * v[x, y]))
                feq[x, y, 4] = rho[x, y] * w[4] * (1 - 3 * v[x, y] + 4.5 * v[x, y] * v[x, y] - 1.5 * (u[x, y] * u[x, y] + v[x, y] * v[x, y]))
                feq[x, y, 5] = rho[x, y] * w[5] * (1 + 3 * (u[x, y] + v[x, y]) + 4.5 * (u[x, y] + v[x, y]) * (u[x, y] + v[x, y]) - 1.5 * (u[x, y] * u[x, y] + v[x, y] * v[x, y]))
                feq[x, y, 6] = rho[x, y] * w[6] * (1 + 3 * (v[x, y] - u[x, y]) + 4.5 * (v[x, y] - u[x, y]) * (v[x, y] - u[x, y]) - 1.5 * (u[x, y] * u[x, y] + v[x, y] * v[x, y]))
                feq[x, y, 7] = rho[x, y] * w[7] * (1 + 3 * (-u[x, y] - v[x, y]) + 4.5 * (u[x, y] + v[x, y]) * (u[x, y] + v[x, y]) - 1.5 * (u[x, y] * u[x, y] + v[x, y] * v[x, y]))
                feq[x, y, 8] = rho[x, y] * w[8] * (1 + 3 * (u[x, y] - v[x, y]) + 4.5 * (u[x, y] - v[x, y])*(u[x, y] - v[x, y]) - 1.5 * (u[x, y] * u[x, y] + v[x, y] * v[x, y]))
                
                for k in range(0, 9):
                    f[x, y, k] = f[x, y, k] * (1 - omega) + omega * feq[x, y, k] #colisão        


def propagacao(f, f_linha, n, m):
    for y in range(m-1, 0, -1):
    	for x in range(0, n-1):
    		f_linha[x, y, 2] = f[x, y-1, 2] 
    		f_linha[x, y, 6] = f[x+1, y-1, 6] 
    
    for y in range(m-1, 0, -1):
    	for x in range(n-1, 0, -1):
    		f_linha[x, y, 1] = f[x-1, y, 1]
    		f_linha[x, y, 5] = f[x-1, y-1, 5]
    
    for y in range(0, m-1):
    	for x in range(n-1, 0, -1):
    		f_linha[x, y, 4] = f[x, y+1, 4]
    		f_linha[x, y, 8] = f[x-1, y+1, 8]
     
    for y in range(0, m-1):
    	for x in range(0, n-1):
    		f_linha[x, y, 3] = f[x+1, y, 3]
    		f_linha[x, y, 7] = f[x+1, y+1, 7]

def cond_contorno(f, f_linha, n, m, u_lid):
    for y in range(0,m):
        for x in range(0, n):   
        	#bounce back on west boundary
        	f_linha[0, y, 1] = f[0, y, 3]
        	f_linha[0, y, 5] = f[0, y, 7]
        	f_linha[0, y, 8] = f[0, y, 6]
    
        	#bounce back on east boundary
        	f_linha[n-1, y, 6] = f[n-1, y, 8]
        	f_linha[n-1, y, 3] = f[n-1, y, 1]
        	f_linha[n-1, y, 7] = f[n-1, y, 5]
            
        	#bounce back on south boundary
        	f_linha[x, 0, 2] = f[x, 0, 4]
        	f_linha[x, 0, 6] = f[x, 0, 8]
        	f_linha[x, 0, 5] = f[x, 0, 7]

    #movin lid, north boundary    
    for i in range(1, n-1):
        rho_n = f[i, m-1, 0] + f[i, m-1, 1] + f[i, m-1, 3] + 2 * (f[i, m-1, 2] + f[i, m-1, 6] + f[i, m-1, 5])
        f_linha[i, m-1, 4] = f[i, m-1, 2] 
        f_linha[i, m-1, 8] = f[i, m-1, 6] + rho_n * u_lid / 6.0
        f_linha[i, m-1, 7] = f[i, m-1, 5] - rho_n * u_lid / 6.0



def rho_u_v(f, rho, u, v, cx, cy, n, m, u_lid):
    tmpx = np.zeros(9)
    tmpy = np.zeros(9)
    
    for x in range(0,n):
        for y in range(0,m):
            rho[x, y] = sum(f[x, y, : ]) 
            rho[x, m-1] = f[x, m-1, 0] + f[x, m-1, 1] + f[x, m-1, 3] + 2 * (f[x, m-1, 2] + f[x, m-1, 6] + f[x, m-1, 5])
            
            for k in range(0, 9):
                tmpx[k] = f[x,y,k] * cx[k]
                tmpy[k] = f[x,y,k] * cy[k]
            
            u[x,y] = (1 / rho[x,y]) * sum(tmpx[:])
            v[x,y] = (1 / rho[x,y]) * sum(tmpy[:])
            u[x, m-1] = u_lid # u_lid = 6m/s, mas u_lbm = 0.1
