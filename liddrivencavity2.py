#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun  2 14:04:28 2020

@author: gustavo
"""

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter

import ldc_funcoes as ldc

rho = np.zeros((100, 100)) 
u = np.zeros((100, 100)) 
v = np.zeros((100, 100)) 

feq = np.zeros((100, 100, 9))
f = np.zeros((100, 100, 9))
f_linha = np.zeros((100, 100, 9)) #para atualizar o vetor na propagação sem sobrepor valores

w = np.array([4/9 , 1/9, 1/9, 1/9, 1/9, 1/36, 1/36, 1/36, 1/36, 1/36])
cx = np.array([0, 1, 0, -1, 0, 1, -1, -1, 1])
cy = np.array([0, 0, 1, 0, -1, 1, 1, -1, -1])

n = 100
m = 100
u_lid = 0.1

alpha = 0.01 #viscosidade cinemática
omega = 53/100 #calculei na mão
# Re = uo * 100 / alpha

#aqui estou setando as condições iniciais
for x in range(0, 100):
    u[x, 99] = 0.1
    for y in range(0, 100): 
        rho[x, y] = 5.0 
        

#começo do loop de tempo
for t in range(0, 400): 
    ldc.colisao(u, v, f, feq, rho, omega, w, cx, cy, n, m)
    ldc.rho_u_v(f, rho, u, v, cx, cy, n, m, u_lid)
    ldc.propagacao(f, f_linha, n, m)
    ldc.cond_contorno(f, f_linha, n, m, u_lid)
    
    tmp = f
    f = f_linha
    f_linha = tmp

X = np.arange(0, 100, 1)
Y = np.arange(0, 100, 1)
X, Y = np.meshgrid(X, Y)
vel = np.zeros((100,100))

for x in range(0,100):
    for y in range(0,100):
        vel[x, y] = u[x, y] * u[x, y] + v[x, y] * v[x, y] 
        vel[x, y] = np.sqrt(vel[x,y])
   
fig = plt.figure()
ax = fig.gca(projection='3d')

surf = ax.plot_surface(X, Y, vel, cmap=cm.coolwarm,
                       linewidth=0, antialiased=False)

ax.set_zlim(-3.0, 3.0)
ax.zaxis.set_major_locator(LinearLocator(10))
ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

fig.colorbar(surf, shrink=0.5, aspect=5)

plt.show()
    