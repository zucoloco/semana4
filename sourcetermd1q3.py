#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun  1 17:01:29 2020

@author: gustavo
"""


import numpy as np
import matplotlib.pylab as plt 

T = np.zeros(100) 
C = np.zeros(100)

feq = np.zeros([100, 3])
geq = np.zeros([100, 3])
S = np.zeros([100, 3])

w = np.array([4/6, 1/6, 1/6])

f = np.zeros([100, 3])
f_linha = np.zeros([100, 3]) #para atualizar o vetor na propagação sem sobrepor valores
g = np.zeros([100, 3])
g_linha = np.zeros([100, 3])

Pr = 1.2
Sc = 2.5
u = 0.5
omega = 1/(2 * u + 0.5)

dC_2 = np.zeros(100)

for t in range(200):

    for x in range(0, 99):
        dC_2[x] = (C[x+1] - 2 * C[x] + C[x-1])
        for k in range(0,3):
            S[x, k] =  (-1) * w[k] * dC_2[x] / 3 
            #S[99, k] = S[98, k]
            
    for x in range(0,100):
        T[x] = sum(f[x, :])
        C[x] = sum(g[x, :])
        
        #como o f é a minha temperatura, a equação é de difusão
        feq[x, 0] = w[0] * T[x] 
        feq[x, 1] = w[1] * T[x] 
        feq[x, 2] = w[2] * T[x] 
        
        #como o g diz respeita à concentração, temos que usar a equação de difusão-advecção
        geq[x, 0] = w[0] * C[x] 
        geq[x, 1] = w[1] * C[x] * (1 + 3 * u)
        geq[x, 2] = w[2] * C[x] * (1 - 3 * u)
        
        for k in range(3):
            f[x, k] = f[x, k] * (1 - omega) + omega * feq[x, k] #colisão
            g[x, k] = g[x, k] * (1 - omega) + omega * geq[x, k] + S[x, k]
    

    for x in range(1, 99): #propagação
        f_linha[x, 1] = f[x+1, 1]
        f_linha[x, 2] = f[x-1, 2]
        g_linha[x, 1] = g[x+1, 1]
        g_linha[x, 2] = g[x-1, 2]
        
    f_linha[0, 0] = f[1, 0];
    f_linha[99, 1] = f[98, 1]
    g_linha[0, 0] = g[1, 0];
    g_linha[99, 1] = g[98, 1]
    
    f_linha[0, 1] = 1.0 - f_linha[0, 0] - f_linha[0, 2]
    g_linha[0, 1] = 1.0 - g_linha[0, 0] - g_linha[0, 2]
    
    tmpf = f
    f = f_linha
    f_linha = tmpf
    
    tmpg = g
    g = g_linha
    g_linha = tmpg
    
plt.plot(T)
plt.show()
plt.plot(C)
plt.show()
    